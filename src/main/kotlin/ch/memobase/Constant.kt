/*
 * Copyright (C) 2020 - present  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.memobase

object Constant {
    const val MEDIA_FOLDER_NAME = "media"
    const val THUMBNAIL_FOLDER_NAME = "thumbnails"
    const val SFTP_BASE_PATH_PROPERTY_NAME = "sftp.basePath"
    const val EXTENSIONS_PROPERTY_NAME = "extensions"
    const val REPORTING_STEP_NAME_PROPERTY_NAME = "reportingStepName"
    const val ENV_PROPERTY_NAME = "env"

    const val RDF_PARSER_LANG = "NTRIPLES"
    const val SFTP_PATH_PREFIX = "sftp:"

    const val REPORTING_STEP_NAME_SUFFIX_THUMBNAIL = "-01-thumbnail"
    const val REPORTING_STEP_NAME_SUFFIX_DIGITAL_OBJECT = "-02-digitalObject"
}
