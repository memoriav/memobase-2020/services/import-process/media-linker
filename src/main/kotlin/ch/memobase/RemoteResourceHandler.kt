/*
 * Copyright (C) 2020 - present  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.exceptions.SftpClientException
import ch.memobase.sftp.SftpClient
import org.apache.logging.log4j.LogManager
import java.io.ByteArrayOutputStream
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.net.HttpURLConnection
import java.net.URI
import java.net.URLEncoder
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.Paths

/**
 * Queries oembed API for videos on Vimeo or Youtube and downloads poster images
 */
class RemoteResourceHandler(private val sftpClient: SftpClient) {
    private val log = LogManager.getLogger(this::class.java)

    companion object {
        private val log = LogManager.getLogger(this::class.java)

        /**
         * Checks if URL points to Vimeo
         *
         * @param url to be scrutinised
         *
         * @return true if URL points to Vimeo
         */
        fun isVimeoUrl(url: String): Boolean {
            val hostName = URI.create(url).toURL().host.lowercase()
            val pattern = "^(www.|)vimeo.com$".toRegex()
            return pattern.containsMatchIn(hostName)
        }

        /**
         * Checks if URL point to Youtube
         *
         * @param url URL to be scrutinised
         *
         * @return true if URL points to Youtube
         */
        fun isYoutubeUrl(url: String): Boolean {
            val hostName = URI.create(url).toURL().host.lowercase()
            val pattern = "^(www.|)youtu\\.?be(.com|)$".toRegex()
            return pattern.containsMatchIn(hostName)
        }

        private fun getOembedObject(urlAsString: String): OembedResponse? {
            val url = URI.create(urlAsString).toURL()
            val outputStream = ByteArrayOutputStream()
            try {
                with(url.openConnection() as HttpURLConnection) {
                    requestMethod = "GET"
                    outputStream.use { _ ->
                        inputStream.copyTo(outputStream)
                    }
                }
            } catch (e: IOException) {
                log.warn("Got a HTTP exception: ${e.message}")
                return null
            } catch (e: FileNotFoundException) {
                log.warn("Couldn't find oembed object on $urlAsString. Is this resource still available?")
                return null
            }
            val result = String(outputStream.toByteArray())
            return OembedResponse.fromJson(result)
        }

        fun createYoutubeOembedUrl(mediaUrl: String): String? {
            val videoIdPattern = "^.*(youtu.be/|v/|u/w/|embed/|watch\\?v=|&v=)([^#&?]*).*".toRegex()
            val videoId = videoIdPattern.find(mediaUrl)?.groups?.get(2)?.value
            return if (videoId != null) {
                "https://youtube.com/oembed?url=" +
                    URLEncoder.encode("https://www.youtube.com/watch?v=$videoId&format=json", StandardCharsets.UTF_8.toString())
            } else {
                null
            }
        }

        fun getYoutubeOembedObject(url: String): OembedResponse? {
            val videoUrl = createYoutubeOembedUrl(url)
            return if (videoUrl != null) {
                getOembedObject(videoUrl)
            } else {
                null
            }
        }

        fun getVimeoOembedObject(url: String): OembedResponse? {
            return getOembedObject(
                "https://vimeo.com/api/oembed.json?url=${
                    URLEncoder.encode(
                        url,
                        StandardCharsets.UTF_8.toString(),
                    )
                }",
            )
        }
    }

    private fun getThumbnail(urlAsString: String): String? {
        val url = URI.create(urlAsString).toURL()
        return try {
            val tempFile = Files.createTempFile("", ".jpg")
            val outputStream = FileOutputStream(tempFile.toFile())
            with(url.openConnection() as HttpURLConnection) {
                requestMethod = "GET"

                outputStream.use { fileOut ->
                    inputStream.copyTo(fileOut)
                }
            }
            tempFile.toString()
        } catch (ex: IOException) {
            log.error("Downloading of thumbnail file failed: ${ex.message}")
            null
        } catch (ex: FileNotFoundException) {
            log.error("Can't find temporary file: ${ex.message}")
            null
        }
    }

    /**
     * Get embedding information and preview image from Youtube
     *
     * @param videoURL URL of video
     * @param downloadThumbnail Fetch thumbnail binary
     *
     * @return Pair with Oembed object and path to local file
     */
    fun getFromYoutube(
        videoURL: String,
        downloadThumbnail: Boolean,
    ): Pair<OembedResponse, String?>? {
        return getYoutubeOembedObject(videoURL)?.let { obj ->
            Pair(
                obj,
                obj.thumbnail_url?.let {
                    if (downloadThumbnail) {
                        log.info("Trying to download thumbnail from $it")
                        getThumbnail(it)
                    } else {
                        log.info("Skipping downloading thumbnail from $it")
                        null
                    }
                },
            )
        }
    }

    /**
     * Get embedding information and preview image from Vimeo
     *
     * @param videoURL URL of video
     * @param downloadThumbnail Fetch thumbnail binary
     *
     * @return Pair with Oembed object and path to local file
     */
    fun getFromVimeo(
        videoURL: String,
        downloadThumbnail: Boolean,
    ): Pair<OembedResponse, String?>? {
        return getVimeoOembedObject(videoURL)?.let { obj ->
            Pair(
                obj,
                obj.thumbnail_url?.let {
                    if (downloadThumbnail) {
                        log.info("Trying to download thumbnail from $it")
                        getThumbnail(it)
                    } else {
                        log.info("Skipping downloading thumbnail from $it")
                        null
                    }
                },
            )
        }
    }

    /**
     * Put file on sFTP server and delete local copy
     *
     * @param sourcePath Path to local temp file
     * @param destPath Path to file about to be created on sFTP server
     * @return destPath or null in case of failure
     */
    fun moveFileToSFTP(
        sourcePath: String,
        destPath: String,
    ): String? {
        return try {
            sftpClient.put(sourcePath, destPath)
            Files.delete(Paths.get(sourcePath))
            destPath
        } catch (ex: IOException) {
            log.warn("Moving thumbnail file to sFTP server on directory $destPath failed: ${ex.message}")
            null
        } catch (ex: SftpClientException) {
            log.warn("Moving thumbnail file to sFTP server on directory $destPath failed: ${ex.message}")
            null
        }
    }
}
