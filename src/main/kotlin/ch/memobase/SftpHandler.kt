/*
 * Copyright (C) 2020 - present  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.reporting.Report
import ch.memobase.reporting.ReportStatus
import ch.memobase.settings.InvalidSettingsValue
import ch.memobase.settings.SettingsLoader
import ch.memobase.sftp.SftpClient
import org.apache.logging.log4j.LogManager

class SftpHandler(
    private val settings: SettingsLoader,
    private val serviceName: String,
) {
    private val log = LogManager.getLogger(this::class.java)
    private val sftpClient = SftpClient(settings.sftpSettings)
    private val fileExtensions = settings.appSettings.getProperty(Constant.EXTENSIONS_PROPERTY_NAME).split(",")

    val sftpBasePath: String =
        run {
            val env = settings.appSettings.getProperty(Constant.ENV_PROPERTY_NAME)
            val basePath = settings.appSettings.getProperty(Constant.SFTP_BASE_PATH_PROPERTY_NAME)
            when (env) {
                "prod" -> {
                    basePath
                }
                "stage", "test" -> {
                    "$basePath/$env"
                }
                else -> {
                    throw InvalidSettingsValue("env", env, "prod, stage or test")
                }
            }
        }

    fun addSftpLocatorToModel(
        key: String,
        container: PipelineContainer,
        type: String,
    ): PipelineContainer {
        val link =
            getLinkToResourceOnSFTPServer(container.headerMetadata.recordSetId, type, container.originalIdentifier)
        return if (link == null) {
            log.warn(
                "No file found in folder $type in record set ${container.headerMetadata.recordSetId} " +
                    "with name ${container.originalIdentifier}.",
            )
            PipelineContainer(
                container.model,
                container.record,
                container.digitalObject,
                container.originalIdentifier,
                container.headerMetadata,
                container.report,
                if (type == Constant.MEDIA_FOLDER_NAME) {
                    Report(
                        key,
                        ReportStatus.warning,
                        "No media file found for id ${container.originalIdentifier}.",
                        serviceName + Constant.REPORTING_STEP_NAME_SUFFIX_DIGITAL_OBJECT,
                    )
                } else {
                    container.digitalObjectReport
                },
                if (type == Constant.THUMBNAIL_FOLDER_NAME) {
                    Report(
                        key,
                        ReportStatus.warning,
                        "No thumbnail found for id ${container.originalIdentifier}.",
                        serviceName + Constant.REPORTING_STEP_NAME_SUFFIX_THUMBNAIL,
                    )
                } else {
                    container.thumbnailReport
                },
            )
        } else {
            return if (type == Constant.MEDIA_FOLDER_NAME) {
                container.model.addLocatorToDigitalObjectResource(link, container.digitalObject)
                PipelineContainer(
                    container.model,
                    container.record,
                    container.digitalObject,
                    container.originalIdentifier,
                    container.headerMetadata,
                    container.report,
                    container.thumbnailReport,
                    Report(
                        key,
                        ReportStatus.success,
                        "Add locator link to digital object $link.",
                        serviceName + Constant.REPORTING_STEP_NAME_SUFFIX_DIGITAL_OBJECT,
                    ),
                )
            } else {
                container.model.createThumbnailResource(container.record, container.digitalObject, link)
                PipelineContainer(
                    container.model,
                    container.record,
                    container.digitalObject,
                    container.originalIdentifier,
                    container.headerMetadata,
                    container.report,
                    container.digitalObjectReport,
                    Report(
                        key,
                        ReportStatus.success,
                        "Found local thumbnail for ${container.originalIdentifier} and created resource.",
                        serviceName + Constant.REPORTING_STEP_NAME_SUFFIX_THUMBNAIL,
                    ),
                )
            }
        }
    }

    private fun getLinkToResourceOnSFTPServer(
        recordSetId: String,
        type: String,
        originalIdentifierValue: String,
    ): String? {
        val transformedIdentifierValue = UtilityFunctions.transformIllegalFilenameCharacters(originalIdentifierValue)
        for (extension in fileExtensions) {
            val filePath = "$sftpBasePath/$recordSetId/$type/$transformedIdentifierValue.$extension"
            if (sftpClient.exists(filePath)) {
                return "${Constant.SFTP_PATH_PREFIX}$filePath"
            }
        }
        log.warn(
            "No media file found. " +
                "Tried $sftpBasePath/$recordSetId/$type/$transformedIdentifierValue.{${fileExtensions.joinToString()}}",
        )
        return null
    }
}
