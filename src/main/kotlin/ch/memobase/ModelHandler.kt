/*
 * Copyright (C) 2020 - present  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.rdf.EBUCORE
import ch.memobase.rdf.RDF
import ch.memobase.rdf.RICO
import ch.memobase.reporting.Report
import ch.memobase.reporting.ReportStatus
import org.apache.jena.rdf.model.Model
import org.apache.jena.rdf.model.ModelFactory
import org.apache.jena.rdf.model.Resource
import org.apache.jena.rdf.model.ResourceFactory
import org.apache.jena.riot.RiotException
import org.apache.logging.log4j.LogManager
import java.io.StringReader
import java.io.StringWriter

class ModelHandler(private val key: String, private val data: String, private val step: String) {
    private val log = LogManager.getLogger(this::class.java)
    val model: Model = ModelFactory.createDefaultModel()
    private val subjects = mutableListOf<Resource>()

    fun populate(): Report {
        return try {
            model.read(StringReader(data), "", Constant.RDF_PARSER_LANG)
            model.listSubjects().forEach {
                subjects.add(it)
            }
            Report(key, ReportStatus.success, "", step)
        } catch (ex: RiotException) {
            log.error("RiotException: ${ex.message}")
            Report(key, ReportStatus.fatal, "RiotException: ${ex.message}", step)
        }
    }

    fun getRecord(): Resource? {
        return subjects.firstOrNull { it.hasProperty(RDF.type, RICO.Record) }
    }

    fun getDigitalInstantiation(): Resource? {
        return subjects.firstOrNull { it.hasProperty(RICO.type, RICO.Types.Instantiation.digitalObject) }
    }

    fun getOriginalIdentifier(record: Resource): String? {
        return record.listProperties(RICO.hasOrHadIdentifier).toList()
            .map { statement -> statement.`object`.asResource() }.firstOrNull { resource ->
                resource.hasProperty(RDF.type, RICO.Identifier) &&
                    resource.hasProperty(
                        RICO.type, RICO.Types.Identifier.original,
                    )
            }?.getProperty(RICO.identifier)?.string
    }

    fun addLocatorToDigitalObjectResource(
        sftpLink: String,
        digitalObjectResource: Resource,
    ) {
        val literal = ResourceFactory.createPlainLiteral(sftpLink)
        digitalObjectResource.addLiteral(EBUCORE.locator, literal)
        model.createTypedLiteral(digitalObjectResource.toString())
        // model.createLiteral(digitalObjectResource.toString(), true)
    }

    fun addDimensionsToDigitalObject(
        digitalObject: Resource,
        oembedObject: OembedResponse,
    ) {
        if (oembedObject.width != null) {
            val width = ResourceFactory.createPlainLiteral(oembedObject.width.toString())
            digitalObject.addLiteral(EBUCORE.width, width)
        }
        if (oembedObject.height != null) {
            val height = ResourceFactory.createPlainLiteral(oembedObject.height.toString())
            digitalObject.addLiteral(EBUCORE.height, height)
        }
        // model.createLiteral(digitalObject.toString(), true)
        model.createTypedLiteral(digitalObject.toString())
    }

    fun createThumbnailResource(
        record: Resource,
        digitalObject: Resource,
        locator: String,
    ): String {
        val uri = "${digitalObject.uri}/derived"
        val thumbnail = model.createResource(uri)
        val literal = ResourceFactory.createPlainLiteral(locator)
        thumbnail.addProperty(RDF.type, RICO.Instantiation)
        thumbnail.addProperty(RICO.type, RICO.Types.Instantiation.thumbnail)
        thumbnail.addProperty(EBUCORE.locator, literal)
        digitalObject.addProperty(RICO.hasDerivedInstantiation, thumbnail)
        thumbnail.addProperty(RICO.isDerivedFromInstantiation, digitalObject)
        record.addProperty(RICO.hasInstantiation, thumbnail)
        thumbnail.addProperty(RICO.isInstantiationOf, record)
        return uri
    }

    fun noThumbnailAttached(): Boolean {
        return model.listSubjects().toList().none { it.hasProperty(RICO.type, RICO.Types.Instantiation.thumbnail) }
    }

    fun hasAccessibleDigitalObjectWithoutLocator(): Boolean {
        return model.listSubjects().toList().any {
            it.hasProperty(
                RICO.type,
                RICO.Types.Instantiation.digitalObject,
            ) && !it.hasProperty(EBUCORE.locator) && it.hasProperty(RICO.isOrWasRegulatedBy) && !isFaroResource()
        }
    }

    fun hasNoAccessProperty(): Boolean {
        return model.listSubjects().toList().any {
            it.hasProperty(
                RICO.type,
                RICO.Types.Instantiation.digitalObject,
            ) && !it.hasProperty(EBUCORE.locator) && !it.hasProperty(RICO.isOrWasRegulatedBy)
        }
    }

    fun isFaroResource(): Boolean {
        return model.listSubjects().toList().firstOrNull { resource ->
            resource.hasProperty(
                RICO.type, RICO.Types.Instantiation.digitalObject,
            ) && resource.hasProperty(RICO.isOrWasRegulatedBy)
        }.let { rule ->
            rule?.listProperties(RICO.isOrWasRegulatedBy)?.toList()?.any { statement ->
                val property = statement.`object`.asResource()
                if (property.hasProperty(RICO.type) &&
                    property.getProperty(RICO.type).literal.string == RICO.Types.Rule.access &&
                    property.hasProperty(RICO.name)
                ) {
                    property.getProperty(RICO.name).literal.string == "faro"
                } else {
                    false
                }
            } ?: false
        }
    }

    fun serialize(): String {
        val out = StringWriter()
        model.write(out, Constant.RDF_PARSER_LANG)
        return out.toString().trim()
    }
}
