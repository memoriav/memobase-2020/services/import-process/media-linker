/*
 * Copyright (C) 2020 - present  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.memobase

import com.beust.klaxon.Klaxon

data class OembedResponse(
    // val type: String,
    // val version: String,
    // val title: String? = null,
    // val author_name: String? = null,
    // val author_url: String? = null,
    // val provider_name: String? = null,
    // val provider_url: String? = null,
    val thumbnail_url: String? = null,
    // val thumbnail_width: Int? = null,
    // val thumbnail_height: Int? = null,
    // val url: String? = null,
    val width: Int? = null,
    val height: Int? = null,
    // val html: String? = null
) {
    companion object {
        fun fromJson(msg: String): OembedResponse? {
            return Klaxon().parse<OembedResponse>(msg)
        }
    }
}
