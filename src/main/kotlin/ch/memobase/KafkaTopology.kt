/*
 * Copyright (C) 2020 - present  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.rdf.EBUCORE
import ch.memobase.reporting.Report
import ch.memobase.reporting.ReportStatus
import ch.memobase.settings.HeaderExtractionSupplier
import ch.memobase.settings.SettingsLoader
import ch.memobase.sftp.SftpClient
import org.apache.kafka.streams.StreamsBuilder
import org.apache.kafka.streams.Topology
import org.apache.kafka.streams.kstream.Branched
import org.apache.kafka.streams.kstream.KStream
import org.apache.kafka.streams.kstream.Named
import org.apache.logging.log4j.LogManager

class KafkaTopology(private val settings: SettingsLoader) {
    private val appSettings = settings.appSettings
    private val log = LogManager.getLogger(this::class.java)

    private val serviceName = appSettings.getProperty(Constant.REPORTING_STEP_NAME_PROPERTY_NAME)
    private val sftpHandler = SftpHandler(settings, serviceName)
    private val sftpClient = SftpClient(settings.sftpSettings)
    private val previewImageHandler = RemoteResourceHandler(sftpClient)
    private val reportingTopic = settings.processReportTopic

    fun build(): Topology {
        val builder = StreamsBuilder()

        val stream = builder.stream<String, String>(settings.inputTopic)

        val loadModel =
            stream
                .processValues(HeaderExtractionSupplier<String>())
                .mapValues { readOnlyKey, value ->
                    val modelHandler = ModelHandler(readOnlyKey, value.first, serviceName)
                    val report = modelHandler.populate()
                    Triple(modelHandler, value.second, report)
                }
                .split(Named.`as`("parser-"))
                .branch({ _, value -> value.third.status == ReportStatus.fatal }, Branched.`as`("fatal"))
                .defaultBranch(Branched.`as`("success"))

        // RDF Parser Failed
        loadModel["parser-fatal"]
            ?.mapValues { value ->
                log.error(value.third.message)
                try {
                    value.third.toJson()
                } catch (ex: Exception) {
                    log.warn("Serialization of report object as JSON failed")
                    ""
                }
            }
            ?.to(reportingTopic)

        // RDF Parser Success
        val extractRequiredEntitiesAndProperty =
            loadModel["parser-success"]?.mapValues { readOnlyKey, value ->
                val model = value.first
                val record =
                    model.getRecord()
                        ?: return@mapValues PipelineContainer(
                            model,
                            model.model.createResource(),
                            model.model.createResource(),
                            "",
                            value.second,
                            Report(
                                readOnlyKey,
                                ReportStatus.fatal,
                                "No record entity found in model.",
                                serviceName,
                            ),
                            Report("", "", "", ""),
                            Report("", "", "", ""),
                        )

                val digitalObject =
                    model.getDigitalInstantiation() ?: return@mapValues PipelineContainer(
                        model,
                        record,
                        model.model.createResource(),
                        "",
                        value.second,
                        Report(
                            readOnlyKey,
                            ReportStatus.ignored,
                            "No digital entity present in model.",
                            serviceName,
                        ),
                        Report("", "", "", ""),
                        Report("", "", "", ""),
                    )
                val originalIdentifierValue =
                    model.getOriginalIdentifier(record) ?: return@mapValues PipelineContainer(
                        model,
                        record,
                        digitalObject,
                        "",
                        value.second,
                        Report(
                            readOnlyKey,
                            ReportStatus.fatal,
                            "No original identifier in record '${record.uri}' present.",
                            serviceName,
                        ),
                        Report("", "", "", ""),
                        Report("", "", "", ""),
                    )
                log.info("Successfully extracted required resources for media linking.")
                PipelineContainer(
                    model,
                    record,
                    digitalObject,
                    originalIdentifierValue,
                    value.second,
                    Report(
                        readOnlyKey,
                        ReportStatus.success,
                        "All required resources are present for processing.",
                        serviceName,
                    ),
                    Report("", "", "", ""),
                    Report("", "", "", ""),
                )
            }
                ?.split(Named.`as`("extraction-"))
                // No record or original identifier. Can't fix this. Stop processing
                ?.branch({ _, value -> value.report.status == ReportStatus.fatal }, Branched.`as`("fatal"))
                // No digital object, no action needed -> send downstream.
                ?.branch({ _, value -> value.report.status == ReportStatus.ignored }, Branched.`as`("ignored"))
                ?.defaultBranch(Branched.`as`("success"))

        // No record or no original identifier.
        extractRequiredEntitiesAndProperty?.get("extraction-fatal")
            ?.mapValues { _, value ->
                log.error(value.report.message)
                try {
                    value.report.toJson()
                } catch (ex: Exception) {
                    log.warn("Serialization of report object as JSON failed")
                    ""
                }
            }
            ?.to(reportingTopic)

        // No digital object -> send onwards without processing
        extractRequiredEntitiesAndProperty?.get("extraction-ignored")
            ?.mapValues { value ->
                log.warn(value.report.message)
                try {
                    value.report.toJson()
                } catch (ex: Exception) {
                    log.warn("Serialization of report object as JSON failed")
                    ""
                }
            }
            ?.to(reportingTopic)

        if (extractRequiredEntitiesAndProperty?.get("extraction-ignored") != null) {
            extractRequiredEntitiesAndProperty["extraction-ignored"]?.let { sendDownstream(it) }
        }

        // processing media resources
        val hasLocatorBranch =
            extractRequiredEntitiesAndProperty?.get("extraction-success")?.mapValues { readOnlyKey, value ->
                addThumbnailSftpLocatorToModel(
                    readOnlyKey,
                    value,
                )
            }
                ?.split(Named.`as`("origin-"))
                // Try loading local media file.
                ?.branch(
                    { _, value -> value.model.hasAccessibleDigitalObjectWithoutLocator() },
                    Branched.`as`("local-media"),
                )
                // No access flag present. Nothing to do.
                ?.branch({ _, value -> value.model.hasNoAccessProperty() }, Branched.`as`("no-access-property"))
                // Is faro resource?
                ?.branch({ _, value -> value.model.isFaroResource() }, Branched.`as`("faro-resource"))
                // Other cases
                ?.defaultBranch(Branched.`as`("other-case"))

        // No locator, but is accessible -> add local media file if possible
        val resourceWithLocalMediaFile =
            hasLocatorBranch?.get("origin-local-media")?.mapValues { readOnlyKey, value ->
                addMediaSftpLocatorToModel(readOnlyKey, value)
            }

        // has no access property -> do nothing. This can be the case when there is a thumbnail but no accessible
        // digital object
        val resourceWithoutAccessProp =
            hasLocatorBranch?.get("origin-no-access-property")?.mapValues { readOnlyKey, value ->
                log.warn("Found a digital object that has no access flags.")
                PipelineContainer(
                    value.model,
                    value.record,
                    value.digitalObject,
                    value.originalIdentifier,
                    value.headerMetadata,
                    value.report,
                    Report(
                        readOnlyKey,
                        ReportStatus.ignored,
                        "Digital object has no access flags no action taken.",
                        serviceName + Constant.REPORTING_STEP_NAME_SUFFIX_DIGITAL_OBJECT,
                    ),
                    value.thumbnailReport,
                )
            }

        // Is faro resource -> do nothing
        val resourceWithFaroMediaFile =
            hasLocatorBranch?.get("origin-faro-resource")?.mapValues { readOnlyKey, value ->
                log.info("Faro resource -> no action taken on digital object as can't be accessed.")
                PipelineContainer(
                    value.model,
                    value.record,
                    value.digitalObject,
                    value.originalIdentifier,
                    value.headerMetadata,
                    value.report,
                    Report(
                        readOnlyKey,
                        ReportStatus.ignored,
                        "Faro resource -> no action taken on digital object as can't be accessed.",
                        serviceName + Constant.REPORTING_STEP_NAME_SUFFIX_DIGITAL_OBJECT,
                    ),
                    value.thumbnailReport,
                )
            }

        // accessible & locator -> check if youtube / vimeo
        val resourceWithoutLocalMediaFile =
            hasLocatorBranch?.get("origin-other-case")
                ?.mapValues { readOnlyKey, value ->
                    fetchThumbnailForYoutubeOrVimeoFile(readOnlyKey, value)
                }

        if (resourceWithLocalMediaFile != null) {
            sendDownstream(resourceWithLocalMediaFile)
            report(resourceWithLocalMediaFile)
        }

        if (resourceWithoutAccessProp != null) {
            sendDownstream(resourceWithoutAccessProp)
            report(resourceWithoutAccessProp)
        }

        if (resourceWithFaroMediaFile != null) {
            sendDownstream(resourceWithFaroMediaFile)
            report(resourceWithFaroMediaFile)
        }

        if (resourceWithoutLocalMediaFile != null) {
            sendDownstream(resourceWithoutLocalMediaFile)
            report(resourceWithoutLocalMediaFile)
        }

        return builder.build()
    }

    private fun fetchThumbnailForYoutubeOrVimeoFile(
        key: String,
        value: PipelineContainer,
    ): PipelineContainer {
        val downloadThumbnail = value.model.noThumbnailAttached()
        val locator = value.digitalObject.getProperty(EBUCORE.locator).string
        when {
            UtilityFunctions.isNoValidUrl(locator) -> {
                log.error("The locator found in record ${value.record.uri} is not valid.")
                return PipelineContainer(
                    value.model,
                    value.record,
                    value.digitalObject,
                    value.originalIdentifier,
                    value.headerMetadata,
                    value.report,
                    Report(
                        key,
                        ReportStatus.warning,
                        "The locator is not a valid URL.",
                        serviceName + Constant.REPORTING_STEP_NAME_SUFFIX_DIGITAL_OBJECT,
                    ),
                    value.thumbnailReport,
                )
            }

            RemoteResourceHandler.isVimeoUrl(locator) -> {
                log.info("Trying to download oembed metadata ${if (downloadThumbnail) "and thumbnail " else ""}from vimeo.")
                val thumbnailHandler = previewImageHandler.getFromVimeo(locator, downloadThumbnail)
                if (thumbnailHandler == null) {
                    log.error("Download from vimeo failed for ${value.record.uri}}!")
                    return PipelineContainer(
                        value.model,
                        value.record,
                        value.digitalObject,
                        value.originalIdentifier,
                        value.headerMetadata,
                        value.report,
                        Report(
                            key,
                            ReportStatus.warning,
                            "Could not download oembed metadata from vimeo",
                            serviceName + Constant.REPORTING_STEP_NAME_SUFFIX_DIGITAL_OBJECT,
                        ),
                        if (downloadThumbnail) {
                            Report(
                                key,
                                ReportStatus.warning,
                                "Could not download thumbnail from vimeo",
                                serviceName + Constant.REPORTING_STEP_NAME_SUFFIX_THUMBNAIL,
                            )
                        } else {
                            value.thumbnailReport
                        },
                    )
                }
                thumbnailHandler
            }

            RemoteResourceHandler.isYoutubeUrl(locator) -> {
                log.info("Trying to download oembed metadata ${if (downloadThumbnail) "and thumbnail " else ""}from youtube.")
                val thumbnailHandler = previewImageHandler.getFromYoutube(locator, downloadThumbnail)
                if (thumbnailHandler == null) {
                    log.error("Download from youtube failed for ${value.record.uri}}!")
                    return PipelineContainer(
                        value.model,
                        value.record,
                        value.digitalObject,
                        value.originalIdentifier,
                        value.headerMetadata,
                        value.report,
                        Report(
                            key,
                            ReportStatus.warning,
                            "Could not download oembed metadata from youtube.",
                            serviceName + Constant.REPORTING_STEP_NAME_SUFFIX_DIGITAL_OBJECT,
                        ),
                        if (downloadThumbnail) {
                            Report(
                                key,
                                ReportStatus.warning,
                                "Could not download thumbnail from youtube.",
                                serviceName + Constant.REPORTING_STEP_NAME_SUFFIX_THUMBNAIL,
                            )
                        } else {
                            value.thumbnailReport
                        },
                    )
                }
                thumbnailHandler
            }

            else -> {
                log.info("No action on remote resource required as not youtube / vimeo.")
                return PipelineContainer(
                    value.model,
                    value.record,
                    value.digitalObject,
                    value.originalIdentifier,
                    value.headerMetadata,
                    value.report,
                    Report(
                        key,
                        ReportStatus.ignored,
                        "No action taken for unknown remote resource.",
                        serviceName + Constant.REPORTING_STEP_NAME_SUFFIX_DIGITAL_OBJECT,
                    ),
                    if (downloadThumbnail) {
                        Report(
                            key,
                            ReportStatus.warning,
                            "No thumbnail fetched for remote resource.",
                            serviceName + Constant.REPORTING_STEP_NAME_SUFFIX_THUMBNAIL,
                        )
                    } else {
                        value.thumbnailReport
                    },
                )
            }
        }.let { h ->
            value.model.addDimensionsToDigitalObject(value.digitalObject, h.first)
            val filePath = h.second
            return if (filePath != null) {
                moveThumbnailToSFTPServer(key, filePath, value)
            } else if (!downloadThumbnail) {
                PipelineContainer(
                    value.model,
                    value.record,
                    value.digitalObject,
                    value.originalIdentifier,
                    value.headerMetadata,
                    value.report,
                    Report(
                        key,
                        ReportStatus.ignored,
                        "No action taken for youtube / vimeo resource.",
                        serviceName + Constant.REPORTING_STEP_NAME_SUFFIX_DIGITAL_OBJECT,
                    ),
                    value.thumbnailReport,
                )
            } else {
                log.error("No thumbnail url enriched for $key.")
                PipelineContainer(
                    value.model,
                    value.record,
                    value.digitalObject,
                    value.originalIdentifier,
                    value.headerMetadata,
                    value.report,
                    Report(
                        key,
                        ReportStatus.ignored,
                        "No action taken for youtube / vimeo resource.",
                        serviceName + Constant.REPORTING_STEP_NAME_SUFFIX_DIGITAL_OBJECT,
                    ),
                    Report(
                        key,
                        ReportStatus.warning,
                        "Download of youtube / vimeo thumbnail failed or no thumbnail available.",
                        serviceName + Constant.REPORTING_STEP_NAME_SUFFIX_THUMBNAIL,
                    ),
                )
            }
        }
    }

    // add thumbnail to sftp server if possible
    private fun moveThumbnailToSFTPServer(
        key: String,
        pathToLocalFile: String,
        container: PipelineContainer,
    ): PipelineContainer {
        val destPath =
            "${sftpHandler.sftpBasePath}/${container.headerMetadata.recordSetId}/${Constant.THUMBNAIL_FOLDER_NAME}/${
                container.originalIdentifier
            }.jpg"
        val pathOnSftpServer = previewImageHandler.moveFileToSFTP(pathToLocalFile, destPath)
        if (pathOnSftpServer != null) {
            log.info("Move downloaded thumbnail file to $destPath for ${container.originalIdentifier}.")
            container.model.createThumbnailResource(
                container.record,
                container.digitalObject,
                "${Constant.SFTP_PATH_PREFIX}$pathOnSftpServer",
            )
            return PipelineContainer(
                container.model,
                container.record,
                container.digitalObject,
                container.originalIdentifier,
                container.headerMetadata,
                container.report,
                Report(
                    key,
                    ReportStatus.ignored,
                    "No action taken for unknown remote resource.",
                    serviceName + Constant.REPORTING_STEP_NAME_SUFFIX_DIGITAL_OBJECT,
                ),
                Report(
                    key,
                    ReportStatus.success,
                    "Added youtube or vimeo thumbnail to sFTP server.",
                    serviceName + Constant.REPORTING_STEP_NAME_SUFFIX_THUMBNAIL,
                ),
            )
        } else {
            log.error("Couldn't move downloaded thumbnail file to $destPath for ${container.originalIdentifier}.")
            return PipelineContainer(
                container.model,
                container.record,
                container.digitalObject,
                container.originalIdentifier,
                container.headerMetadata,
                container.report,
                Report(
                    key,
                    ReportStatus.ignored,
                    "No action taken for unknown remote resource.",
                    serviceName + Constant.REPORTING_STEP_NAME_SUFFIX_DIGITAL_OBJECT,
                ),
                Report(
                    key,
                    ReportStatus.warning,
                    "Upload of youtube / vimeo thumbnail to sFTP server failed",
                    serviceName + Constant.REPORTING_STEP_NAME_SUFFIX_THUMBNAIL,
                ),
            )
        }
    }

    private fun addThumbnailSftpLocatorToModel(
        key: String,
        container: PipelineContainer,
    ): PipelineContainer {
        return sftpHandler.addSftpLocatorToModel(key, container, Constant.THUMBNAIL_FOLDER_NAME)
    }

    private fun addMediaSftpLocatorToModel(
        key: String,
        container: PipelineContainer,
    ): PipelineContainer {
        return sftpHandler.addSftpLocatorToModel(key, container, Constant.MEDIA_FOLDER_NAME)
    }

    private fun report(kstream: KStream<String, PipelineContainer>) {
        kstream
            .mapValues { _, value -> value.report.toJson() }
            .to(reportingTopic)

        kstream
            .mapValues { _, value -> value.digitalObjectReport.toJson() }
            .to(reportingTopic)

        kstream
            .mapValues { _, value -> value.thumbnailReport.toJson() }
            .to(reportingTopic)
    }

    private fun sendDownstream(kstream: KStream<String, PipelineContainer>) {
        kstream
            .mapValues { value -> value.model.serialize() }
            .to(settings.outputTopic)
    }
}
