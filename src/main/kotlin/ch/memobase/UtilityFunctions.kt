/*
 * Copyright (C) 2020 - present  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.memobase

import java.net.MalformedURLException
import java.net.URI

object UtilityFunctions {
    /**
     * This utility function is used to replace illegal characters which cannot be used in filenames.
     */
    fun transformIllegalFilenameCharacters(input: String): String {
        return input.replace("/", "_")
    }

    /**
     * Parses the string as URL to ensure a base validity for the locator.
     */
    fun isNoValidUrl(locator: String): Boolean {
        return try {
            URI.create(locator).toURL()
            false
        } catch (ex: IllegalArgumentException) {
            true
        } catch (ex: MalformedURLException) {
            true
        }
    }
}
