/*
 * Copyright (C) 2020 - present Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.reporting.Report
import ch.memobase.testing.EmbeddedSftpServer
import com.beust.klaxon.Klaxon
import org.apache.kafka.common.header.internals.RecordHeader
import org.apache.kafka.common.header.internals.RecordHeaders
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.kafka.common.serialization.StringSerializer
import org.apache.kafka.streams.TestOutputTopic
import org.apache.kafka.streams.TopologyTestDriver
import org.apache.kafka.streams.test.TestRecord
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.assertAll
import org.junit.jupiter.api.assertDoesNotThrow
import java.io.File
import java.io.FileInputStream
import java.nio.charset.Charset
import java.nio.file.Paths

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestKafkaTopology {
    private val resourcePath = "src/test/resources/data"

    private fun readFile(
        fileName: String,
        count: Int,
    ): String {
        return File("$resourcePath/test$count/$fileName").readText(Charset.defaultCharset())
    }

    private fun readAndSortNtTriplesFile(
        fileName: String,
        count: Int,
    ): String {
        val content = File("$resourcePath/test$count/$fileName").readText(Charset.defaultCharset())
        return sortNTriples(content)
    }

    private fun sortNTriples(content: String): String {
        return content.split("\n").sorted().joinToString("\n")
    }

    private val sftpServer = EmbeddedSftpServer(22000, "user", "password")
    private val klaxon = Klaxon()
    private val config = "test1.yml"

    init {
        val media =
            listOf(
                Pair("/base/test-record-set-id/media", "test-1.jpg"),
                Pair("/base/test-record-set-id/media", "test-6.jpg"),
            )

        val thumbnails =
            listOf(
                Pair("/base/test-record-set-id/thumbnails", "test-1.jpg"),
                Pair("/base/test-record-set-id/thumbnails", "test-5.jpg"),
                Pair("/base/test-record-set-id/thumbnails", "test-6.jpg"),
                Pair("/base/test-record-set-id/thumbnails", "test-7.jpg"),
                Pair("/base/test-record-set-id/thumbnails", "test-9.jpg"),
                Pair("/base/test-record-set-id/thumbnails", "test-11.png"),
            )

        for (pair in media) {
            sftpServer.putFile(
                Paths.get(pair.first, pair.second).toString(),
                FileInputStream(Paths.get("src/test/resources/data/media", pair.second).toFile()),
            )
        }
        for (pair in thumbnails) {
            sftpServer.putFile(
                Paths.get(pair.first, pair.second).toString(),
                FileInputStream(Paths.get("src/test/resources/data/thumbnails", pair.second).toFile()),
            )
        }
    }

    private fun parseReport(data: String): Report {
        return klaxon.parse<Report>(data)!!
    }

    private fun setupTest(
        fileName: String,
        count: Int,
        inputKey: String,
    ): Pair<TestOutputTopic<String, String>, TestOutputTopic<String, String>> {
        val settings = App.defineSettings(fileName)
        val testDriver =
            TopologyTestDriver(KafkaTopology(settings).build(), settings.kafkaStreamsSettings)
        val inputValue = readFile("input$count.nt", count)
        val headers = RecordHeaders()
        headers.add(RecordHeader("sessionId", "test-session-id".toByteArray()))
        headers.add(RecordHeader("recordSetId", "test-record-set-id".toByteArray()))
        headers.add(RecordHeader("institutionId", "test-institution-id".toByteArray()))
        val inputTopic =
            testDriver.createInputTopic(settings.inputTopic, StringSerializer(), StringSerializer())
        val inputRecord = TestRecord(inputKey, inputValue, headers)
        inputTopic.pipeInput(inputRecord)
        val outputTopic =
            testDriver.createOutputTopic(settings.outputTopic, StringDeserializer(), StringDeserializer())
        val outputReportTopic =
            testDriver.createOutputTopic(
                settings.processReportTopic,
                StringDeserializer(),
                StringDeserializer(),
            )
        return Pair(outputTopic, outputReportTopic)
    }

    private fun assertEmptyOutput(
        report: Report,
        isEmpty: Boolean,
        reportCount: Int,
        id: String,
        status: String,
        message: String,
    ) {
        assertAll({
            assertThat(isEmpty).isTrue()
        }, {
            assertThat(reportCount).isEqualTo(1)
        }, {
            assertThat(report.id).isEqualTo(id)
        }, {
            assertThat(report.status).isEqualTo(status)
        }, {
            assertThat(report.message).isEqualTo(message)
        })
    }

    private fun assertOutputWithSingleReport(
        key: String,
        value: String,
        report: Report,
        reportCount: Int,
        id: String,
        status: String,
        message: String,
        count: Int,
    ) {
        assertAll({
            assertThat(key).isEqualTo("https://memobase.ch/record/mav-001-test-x")
        }, {
            assertThat(sortNTriples(value)).isEqualTo(readAndSortNtTriplesFile("output$count.nt", count))
        }, {
            assertThat(reportCount).isEqualTo(1)
        }, {
            assertThat(report.id).isEqualTo(id)
        }, {
            assertThat(report.status).isEqualTo(status)
        }, {
            assertThat(report.message).isEqualTo(message)
        })
    }

    private fun assertOutputWithTripleReports(
        recordKey: String,
        resultKey: String,
        resultValue: String,
        reports: List<Report>,
        reportCount: Int,
        count: Int,
    ) {
        assertAll("", {
            assertThat(resultKey).isEqualTo(recordKey)
        }, {
            assertThat(sortNTriples(resultValue)).isEqualTo(
                readAndSortNtTriplesFile("output$count.nt", count),
            )
        }, {
            assertThat(reportCount).isEqualTo(3)
        }, {
            assertThat(reports[0]).isEqualTo(parseReport(readFile("output$count-0.json", count)))
        }, {
            assertThat(reports[1]).isEqualTo(parseReport(readFile("output$count-1.json", count)))
        }, {
            assertThat(reports[2]).isEqualTo(parseReport(readFile("output$count-2.json", count)))
        })
    }

    @Test
    fun `test 1 invalid input message`() {
        val output = setupTest(config, 1, "https://memobase.ch/record/mav-001-test-x")
        val reports = output.second.readRecordsToList()
        assertEmptyOutput(
            parseReport(reports[0].value),
            output.first.isEmpty,
            reports.size,
            "https://memobase.ch/record/mav-001-test-x",
            "FATAL",
            "RiotException: [line: 1, col: 1 ] Expected BNode or IRI: Got: [KEYWORD:Some]",
        )
    }

    @Test
    fun `test 2 missing record entity`() {
        val output = setupTest(config, 2, "https://memobase.ch/record/mav-001-test-x")
        val reports = output.second.readRecordsToList()
        assertEmptyOutput(
            parseReport(reports[0].value),
            output.first.isEmpty,
            reports.size,
            "https://memobase.ch/record/mav-001-test-x",
            "FATAL",
            "No record entity found in model.",
        )
    }

    @Test
    fun `test 3 ignore record without digital instantiation`() {
        val output = setupTest(config, 3, "https://memobase.ch/record/mav-001-test-x")
        val record = output.first.readRecord()
        val reports = output.second.readRecordsToList()
        assertOutputWithSingleReport(
            record.key,
            record.value,
            parseReport(reports[0].value),
            reports.size,
            "https://memobase.ch/record/mav-001-test-x",
            "IGNORE",
            "No digital entity present in model.",
            3,
        )
    }

    @Test
    fun `test 4 record without original identifier`() {
        val output = setupTest(config, 4, "https://memobase.ch/record/mav-001-test-x")
        val reports = output.second.readRecordsToList()
        assertEmptyOutput(
            parseReport(reports[0].value),
            output.first.isEmpty,
            reports.size,
            "https://memobase.ch/record/mav-001-test-x",
            "FATAL",
            "No original identifier in record 'https://memobase.ch/record/test1' present.",
        )
    }

    @Test
    fun `test 5 record with digital object and original identifier, but nothing else`() {
        val output = setupTest(config, 5, "https://memobase.ch/record/mav-001-test-x")
        val record = output.first.readRecord()
        val reports = output.second.readRecordsToList().map { value -> parseReport(value.value) }
        assertOutputWithTripleReports(
            "https://memobase.ch/record/mav-001-test-x",
            record.key,
            record.value,
            reports,
            reports.size,
            5,
        )
    }

    @Test
    fun `test 6 record with digital object and original identifier without locator but access rule`() {
        val output = setupTest(config, 6, "https://memobase.ch/record/mav-001-test-x")
        val record = output.first.readRecord()
        val reports = output.second.readRecordsToList().map { value -> parseReport(value.value) }
        assertOutputWithTripleReports(
            "https://memobase.ch/record/mav-001-test-x",
            record.key,
            record.value,
            reports,
            reports.size,
            6,
        )
    }

    @Test
    fun `test 7 digital object with faro access rule`() {
        val output = setupTest(config, 7, "https://memobase.ch/record/mav-001-test-x")
        val record = output.first.readRecord()
        val reports = output.second.readRecordsToList().map { value -> parseReport(value.value) }
        assertOutputWithTripleReports(
            "https://memobase.ch/record/mav-001-test-x",
            record.key,
            record.value,
            reports,
            reports.size,
            7,
        )
    }

    @Test
    fun `test 8 digital object with locator, but missing file`() {
        val output = setupTest(config, 8, "https://memobase.ch/record/mav-001-test-x")
        val record = output.first.readRecord()
        val reports = output.second.readRecordsToList().map { value -> parseReport(value.value) }
        assertOutputWithTripleReports(
            "https://memobase.ch/record/mav-001-test-x",
            record.key,
            record.value,
            reports,
            reports.size,
            8,
        )
    }

    @Test
    fun `test 9 invalid locator`() {
        val output = setupTest(config, 9, "https://memobase.ch/record/mav-001-test-x")
        val record = output.first.readRecord()
        val reports = output.second.readRecordsToList().map { value -> parseReport(value.value) }
        assertOutputWithTripleReports(
            "https://memobase.ch/record/mav-001-test-x",
            record.key,
            record.value,
            reports,
            reports.size,
            9,
        )
    }

    @Test
    fun `test 10 youtube locator, no local thumbnail`() {
        val output = setupTest(config, 10, "https://memobase.ch/record/mav-001-test-x")
        val record = output.first.readRecord()
        val reports = output.second.readRecordsToList().map { value -> parseReport(value.value) }
        assertOutputWithTripleReports(
            "https://memobase.ch/record/mav-001-test-x",
            record.key,
            record.value,
            reports,
            reports.size,
            10,
        )
    }

    @Test
    fun `test 11 youtube locator, local thumbnail`() {
        val output = setupTest(config, 11, "https://memobase.ch/record/mav-001-test-x")
        val record = output.first.readRecord()
        val reports = output.second.readRecordsToList().map { value -> parseReport(value.value) }
        assertOutputWithTripleReports(
            "https://memobase.ch/record/mav-001-test-x",
            record.key,
            record.value,
            reports,
            reports.size,
            11,
        )
    }

    @Test
    fun `test 12 invalid locator`() {
        val output = setupTest(config, 12, "https://memobase.ch/record/mav-001-test-x")
        assertDoesNotThrow { output.first.readRecord() }
    }
}
