/*
 * Copyright (C) 2020 - present Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import org.assertj.core.api.AssertionsForClassTypes.assertThat
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertAll

internal class TestRemoteResourceHandler {
    @Test
    fun isYoutubeResource() {
        val url1 = "https://www.youtube.com/embed/xlp4AXb3pDY"
        val url2 = "https://youtube.com/embed/x1p4AXb4pDY"
        val url3 = "https://youtu.be/x1p4AXb4pDY"
        val url4 = "https://www.youtu.be/x1p4AXb4pDY"
        assert(RemoteResourceHandler.isYoutubeUrl(url1))
        assert(RemoteResourceHandler.isYoutubeUrl(url2))
        assert(RemoteResourceHandler.isYoutubeUrl(url3))
        assert(RemoteResourceHandler.isYoutubeUrl(url4))
    }

    @Test
    fun isVimeoResource() {
        val url1 = "https://www.vimeo.com/xlp4AXb3pDY"
        val url2 = "https://vimeo.com/x1p4AXb4pDY"
        assert(RemoteResourceHandler.isVimeoUrl(url1))
        assert(RemoteResourceHandler.isVimeoUrl(url2))
    }

    @Test
    fun `create correct youtube oembed urls from media urls`() {
        val expected =
            "https://youtube.com/oembed?url=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3Da62I4Q6PdVI%26format%3Djson"
        val url1 = "https://www.youtube.com/embed/a62I4Q6PdVI"
        val url2 = "https://youtube.com/embed/a62I4Q6PdVI"
        val url3 = "https://youtu.be/a62I4Q6PdVI"
        val url4 = "https://www.youtu.be/a62I4Q6PdVI"
        val url5 = "https://www.youtube.com/watch?v=a62I4Q6PdVI"
        val url6 = "https://youtube.com/watch?v=a62I4Q6PdVI"
        assertAll(
            { assertThat(RemoteResourceHandler.createYoutubeOembedUrl(url1)).isEqualTo(expected) },
            { assertThat(RemoteResourceHandler.createYoutubeOembedUrl(url2)).isEqualTo(expected) },
            { assertThat(RemoteResourceHandler.createYoutubeOembedUrl(url3)).isEqualTo(expected) },
            { assertThat(RemoteResourceHandler.createYoutubeOembedUrl(url4)).isEqualTo(expected) },
            { assertThat(RemoteResourceHandler.createYoutubeOembedUrl(url5)).isEqualTo(expected) },
            { assertThat(RemoteResourceHandler.createYoutubeOembedUrl(url6)).isEqualTo(expected) },
        )
    }

    @Test
    fun getYoutubeOembedObject() {
        val oembed = RemoteResourceHandler.getYoutubeOembedObject("https://www.youtube.com/watch?v=5ujk7IamcPI")
        assertNotNull(oembed)
    }

    @Test
    fun getVimeoOembedObject() {
        val oembed = RemoteResourceHandler.getVimeoOembedObject("https://vimeo.com/223023510")
        assertNotNull(oembed)
    }

    @Test
    fun getOembedObjectWithInvalidUrl() {
        val oembed = RemoteResourceHandler.getVimeoOembedObject("https://vieo.com/223023510")
        assertNull(oembed)
    }
}
