/*
 * Copyright (C) 2020 - present Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.rdf.EBUCORE
import ch.memobase.rdf.NS
import ch.memobase.rdf.RDF
import ch.memobase.rdf.RICO
import ch.memobase.rdf.RICO.Types.Instantiation.digitalObject
import org.apache.jena.rdf.model.Model
import org.apache.jena.rdf.model.ModelFactory
import org.apache.jena.rdf.model.Resource
import org.apache.jena.riot.RDFDataMgr
import org.apache.jena.riot.RDFFormat
import java.io.FileOutputStream

/**
 * This script can be used to change / generate the input messages in a human readable fashion.
 */

fun main() {
    input3content()
    input4content()
    input5content()
    input6content()
    input7content()
    input8content()
    input9content()
    input10content()
    input11content()
}

fun input3content() {
    val model = ModelFactory.createDefaultModel()
    createRecord(model)
    write(model, 3)
}

fun input4content() {
    val model = ModelFactory.createDefaultModel()
    createRecord(model)
    createDigitalInstantiation(model, true, "https://www.youtube.com/watch?v=5ujk7IamcPI")
    write(model, 4)
}

fun input5content() {
    val model = ModelFactory.createDefaultModel()
    val record = createRecord(model)
    createIdentifier(model, record, "test-5")
    createDigitalInstantiation(model, false, "")
    write(model, 5)
}

fun input6content() {
    val model = ModelFactory.createDefaultModel()
    val record = createRecord(model)
    createIdentifier(model, record, "test-6")
    val digitalObject = createDigitalInstantiation(model, false, "")
    addAccessRuleToDigitalObject(model, digitalObject, "public")
    write(model, 6)
}

fun input7content() {
    val model = ModelFactory.createDefaultModel()
    val record = createRecord(model)
    createIdentifier(model, record, "test-7")
    val digitalObject = createDigitalInstantiation(model, false, "")
    addAccessRuleToDigitalObject(model, digitalObject, "faro")
    write(model, 7)
}

fun input8content() {
    val model = ModelFactory.createDefaultModel()
    val record = createRecord(model)
    createIdentifier(model, record, "test-8")
    val digitalObject = createDigitalInstantiation(model, false, "")
    addAccessRuleToDigitalObject(model, digitalObject, "public")
    write(model, 8)
}

fun input9content() {
    val model = ModelFactory.createDefaultModel()
    val record = createRecord(model)
    createIdentifier(model, record, "test-9")
    val digitalObject = createDigitalInstantiation(model, true, "this is an invalid locator.")
    addAccessRuleToDigitalObject(model, digitalObject, "public")
    write(model, 9)
}

fun input10content() {
    val model = ModelFactory.createDefaultModel()
    val record = createRecord(model)
    createIdentifier(model, record, "test-10")
    val digitalObject = createDigitalInstantiation(model, true, "https://www.youtube.com/watch?v=5ujk7IamcPI")
    addAccessRuleToDigitalObject(model, digitalObject, "public")
    write(model, 10)
}

fun input11content() {
    val model = ModelFactory.createDefaultModel()
    val record = createRecord(model)
    createIdentifier(model, record, "test-11")
    val digitalObject = createDigitalInstantiation(model, true, "https://vimeo.com/216625101")
    addAccessRuleToDigitalObject(model, digitalObject, "public")
    write(model, 11)
}

fun createRecord(model: Model): Resource {
    val record = model.createResource(NS.mbr + "test1")
    record.addProperty(RDF.type, RICO.Record)
    return record
}

fun createIdentifier(
    model: Model,
    record: Resource,
    identifierValue: String,
) {
    val identifier = model.createResource(NS.internal + "identifier1")
    identifier.addProperty(RDF.type, RICO.Identifier)
    identifier.addProperty(RICO.type, RICO.Types.Identifier.original)
    identifier.addProperty(RICO.identifier, identifierValue)
    identifier.addProperty(RICO.isOrWasIdentifierOf, record)
    record.addProperty(RICO.hasOrHadIdentifier, identifier)
}

fun createDigitalInstantiation(
    model: Model,
    hasLocator: Boolean,
    locator: String,
): Resource {
    val digitalObject = model.createResource(NS.mbdo + "test1-do")
    digitalObject.addProperty(RDF.type, RICO.Instantiation)
    digitalObject.addProperty(RICO.type, RICO.Types.Instantiation.digitalObject)
    if (hasLocator) {
        digitalObject.addProperty(EBUCORE.locator, locator)
    }
    return digitalObject
}

fun addAccessRuleToDigitalObject(
    model: Model,
    digitalObject: Resource,
    name: String,
) {
    val rule = model.createResource(NS.internal + "accessRule")
    rule.addProperty(RICO.type, "access")
    rule.addProperty(RICO.name, name)
    digitalObject.addProperty(RICO.isOrWasRegulatedBy, rule)
}

fun write(
    model: Model,
    count: Int,
) {
    RDFDataMgr.write(
        FileOutputStream("src/test/resources/data/test$count/input$count.nt"),
        model,
        RDFFormat.NTRIPLES_UTF8,
    )
}
