## Media Linker

This service is responsible to link media files and preview images with their record. The service creates RDF
resources as necessary for these digital objects. The service defines the ebucore:locator property as necessary to ensure
that subsequent services can find the media and preview files.

[Confluence](https://ub-basel.atlassian.net/wiki/spaces/MEMOBASE/pages/83984396/Service+Media-Linker)
